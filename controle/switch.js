const imprimirResultado = function (nota) {            //Sem break ele executa todos embaixo
    switch (Math.floor(nota)) {
        case 10:
        case 9:
            console.log('Quadro de Honra')
            break
        case 8: case 7:
            console.log('Aprovado')
            break   
        case 6: case 5: case 4:
            console.log('Recuperação')
            break
        case 3, 2, 1, 0:                              //outra forma
            console.log('Reprovado')
            break
        default:
            console.log('Nota Inválida')
    }
    console.log('Fim')
}

imprimirResultado(10)
imprimirResultado(8.9)
imprimirResultado(6.55)
imprimirResultado(2.3)
imprimirResultado(-1)
imprimirResultado(11)